const express = require('express')
const app = express()
const ffhq = require('./data/ffhq-dataset-v2.json')
const ffhq_length = Object.keys(ffhq).length
// const port = 8000
app.use(express.static('www'))


app.get('/api/randomFace', (req, res) => {
  const ran = Math.floor(Math.random() * ffhq_length)
  // console.log(ffhq[ran])
  res.json(ffhq[ran])
})


const listener = app.listen(process.env.PORT, () => {
  console.log("Your app is listening on port " + listener.address().port);
});
